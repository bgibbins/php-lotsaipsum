# php-lotsaipsum


Lorem ipsum generator in PHP that allows for custom word files to be used instead of the regular boring-ipsum.

## Origins

Once upon a time, I was attempting to find a lorem ipsum generator *again* to generate cupcake ipsum, or bacon ipsum, or something-else-different-ipsum. I stumbled across Josh Sherman's fine implementation of a php generator at https://github.com/joshtronic/php-loremipsum. I forked it and fiddled around with it a bit, but eventually came to realize what I wanted was a generator that would allow me to change the source words, perhaps within the same page even... who knows. Consequently I decided that it was too much for a forkful. :)

So, lotsa-ipsum was born out of php-loremipsum with the idea that it could use different source word files and add to the available options over time. This project owes most of the footwork to the original php-loremipsum by Josh Sherman.

## Installation

The preferred installation method is via `composer`. First add the following
to your `composer.json`

```json
"require": {
    "briangibbins/php-lotsaipsum": "dev-master"
}
```

Then run `composer update`

## Usage

### Getting Started (with just boring ipsum, the default)

```php
$lipsum = new briangibbins\LotsaIpsum();
```


### To get started by using a non-lorem word set
Where the word set is a string containing the name of the word set from the src/lang directory (without the .json extension).

```php
$lipsum = new briangibbins\LotsaIpsum('elvish');
```

### To change to a different word set after initializing

```php
$lipsum->useWords('elvish');
```

### Word Sets
- elvish (from Tolkien - Quenya actually)
- lotr (non-Elvish Tolkien)
- meat (like bacon and roast)
- patriot (like your uncle during Thanksgiving)
- lorem (just regular boring stuff)


The following instructions are from Josh Sherman's original php-loremipsum. Why reinvent the wheel? Thanks Josh.

### Generating Words

```php
echo '1 word: '  . $lipsum->word();
echo '5 words: ' . $lipsum->words(5);
```

### Generating sentences

```php
echo '1 sentence:  ' . $lipsum->sentence();
echo '5 sentences: ' . $lipsum->sentences(5);
```

### Generating paragraphs

```php
echo '1 paragraph:  ' . $lipsum->paragraph();
echo '5 paragraphs: ' . $lipsum->paragraphs(5);
```

### Wrapping text with HTML tags

If you would like to wrap the generated text with a tag, pass it as the second
parameter:

```php
echo $lipsum->paragraphs(3, 'p');

// Generates: <p>Lorem ipsum...</p><p>...</p><p>...</p>
```

Multiple tags can also be specified:

```php
echo $lipsum->sentences(3, ['article', 'p']);

// Generates: <article><p>...</p></article><article><p>...</p></article><article><p>...</p></article>
```

And you can back reference using `$1`:

```php
echo $lipsum->words(3, '<li><a href="$1">$1</a></li>');

// Generates: <li><a href="...">...</a></li><li><a href="...">...</a></li><li><a href="...">...</a></li>
```

### Return as an array

Perhaps you want an array instead of a string:

```php
print_r($lipsum->wordsArray(5));
print_r($lipsum->sentencesArray(5));
print_r($lipsum->paragraphsArray(5));
```

You can still wrap with markup when returning an array:

```php
print_r($lipsum->wordsArray(5), 'li');
```
