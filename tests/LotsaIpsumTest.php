<?php
require_once 'vendor/autoload.php';

class LotsaIpsumTest extends PHPUnit\Framework\TestCase
{
    private $lipsum;

    public function setUp()
    {
        $this->lipsum = new briangibbins\LotsaIpsum();
    }

    public function testListWordFiles()
    {
	    $words = $this->lipsum->listWordFiles();
	    $this->assertInternalType('array', $words);
	    $this->assertArrayHasKey('lorem',$words);
    }


    public function testLoadWords()
    {
	    $words = $this->lipsum->listWordFiles();
	    foreach ($words as $key => $value) {
		    $this->assertFileIsReadable(__DIR__ . '/../src/lang/' . $key . '.json');
	    }
    }

    public function testFilesFormat()
    {
	    $words = $this->lipsum->listWordFiles();
	    foreach ($words as $key => $value) {
		    $filepath = __DIR__ . '/../src/lang/' . $key . '.json';
		    $data = json_decode(file_get_contents($filepath),true);
		    $this->assertInternalType('array', $data);
	    }
    }

    public function testIsWordGroupNameTrue()
	{
		$this->assertTrue($this->lipsum->isWordGroupName("lorem"));
    }

    public function testIsWordGroupNameFalse()
    {
	    $this->assertFalse($this->lipsum->isWordGroupName("incorrect"));
    }

    public function testUseWordsElvish() {
	    $this->assertTrue($this->lipsum->useWords("elvish"));
    }

	public function testUseWordsIncorrect() {
		$this->assertFalse($this->lipsum->useWords("incorrect"));
	}


    public function testWord()
    {
        $this->assertRegExp('/^.+$/i', $this->lipsum->word());

    }

    public function testWords()
    {
        $this->assertRegExp(
            '/^.+ .+ .+$/i',
            $this->lipsum->words(3)
        );
    }

    public function testWordsArray()
    {
        $words = $this->lipsum->wordsArray(3);
        $this->assertTrue(is_array($words));
        $this->assertCount(3, $words);

        foreach ($words as $word)
        {
            $this->assertRegExp('/^.+$/i', $word);
        }
    }

    public function testWordsExceedingVocab()
    {
        $this->assertCount(500, $this->lipsum->wordsArray(500));
    }

    public function testSentence()
    {
        $this->assertRegExp('/^.+\.$/i', $this->lipsum->sentence());
    }

    public function testSentences()
    {
        $this->assertRegExp('/^.+\. .+\. .+\.$/i', $this->lipsum->sentences(3));
    }

    public function testSentencesArray()
    {
        $sentences = $this->lipsum->sentencesArray(3);
        $this->assertTrue(is_array($sentences));
        $this->assertCount(3, $sentences);

        foreach ($sentences as $sentence)
        {
            $this->assertRegExp('/^.+\.$/i', $sentence);
        }
    }

    public function testParagraph()
    {
        $this->assertRegExp('/^(.+\.)+$/i', $this->lipsum->paragraph());
    }

    public function testParagraphs()
    {
        $this->assertRegExp(
            '/^(.+\.)+\n\n(.+\.)+\n\n(.+\.)+$/i',
            $this->lipsum->paragraphs(3)
        );
    }

    public function testParagraphsArray()
    {
        $paragraphs = $this->lipsum->paragraphsArray(3);
        $this->assertTrue(is_array($paragraphs));
        $this->assertCount(3, $paragraphs);

        foreach ($paragraphs as $paragraph)
        {
            $this->assertRegExp('/^(.+\.)+$/i', $paragraph);
        }
    }

    public function testMarkupString()
    {
        $this->assertRegExp(
            '/^<li>..+<\/li>$/i',
            $this->lipsum->word('li')
        );
    }

    public function testMarkupArray()
    {
        $this->assertRegExp(
            '/^<div><p>.+<\/p><\/div>$/i',
            $this->lipsum->word(array('div', 'p'))
        );
    }

    public function testMarkupBackReference()
    {
        $this->assertRegExp(
            '/^<li><a href=".+">.+<\/a><\/li>$/i',
            $this->lipsum->word('<li><a href="$1">$1</a></li>')
        );
    }

    public function testMarkupArrayReturn()
    {
        $words = $this->lipsum->wordsArray(3, 'li');
        $this->assertTrue(is_array($words));
        $this->assertCount(3, $words);

        foreach ($words as $word)
        {
            $this->assertRegExp('/^<li>.+<\/li>$/i', $word);
        }
    }
}

