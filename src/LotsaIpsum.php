<?php

/**
 * Lorem Ipsum Generator
 *
 * PHP version 5.3+
 *
 * Licensed under The MIT License.
 * Redistribution of these files must retain the above copyright notice.
 *
 * @author    Brian Gibbins <brian@briangibbins.com>
 * @copyright Copyright 2017, Brian Gibbins
 * @license   http://www.opensource.org/licenses/mit-license.html
 * @link      https://bitbucket.org/bgibbins/php-lotsaipsum
 */

namespace briangibbins;

class LotsaIpsum
{
    /**
     * First
     *
     * Whether or not we should be starting the string with the first two words in the word group (a/k/a lorem ipsum)
     *
     * @access private
     * @var    boolean
     */
    private $first = TRUE;

    /**
     * Words
     *
     * An empty array to fill with words
     *
     * @access private
     * @var    array
     */
    public $words = array();

    /**
     * LotsaIpsum constructor.
     * @param null $type string
     * Sets the initial word list to use defaulting to boring-ipsum
     */
    public function __construct($type = null)
    {
        if (!empty($type)) {
            $words = $this->loadWords($type);
            if ($words !== false) {
                $filler = $this->getFiller();
                $words = array_merge($words, $filler);
                $this->words = $words;
            } else {
                error_log("Unable to create custom word array.");
                exit;
            }
        } else {
            //Actual lorem ipsum doesn't use filler text¨
            $words = $this->loadWords("lorem");
            if ($words !== false) {
                $this->words = $words;
            } else {
                error_log("Unable to create default word array.");
                exit;
            }
        }
    }

    /**
     * useWords
     *
     * Public function allowing for word set to be modified after constructed.
     *
     * @param $wordGroup string
     * @return boolean
     */
    public function useWords($wordGroup)
    {
        $words = $this->loadWords($wordGroup);
        if ($words !== false) {
            $this->words = $words;
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /*
     * listWordFiles
     *
     * Lists valid word file partials that can be used for specifying w/ useWords() & loadWords().
     *
     * @return array
     */
    public function listWordFiles()
    {
        $filesArray = Array(
            "elvish"  => "Quendar Elvish (Tolkien)",
            "meat"    => "Meat Toppings Special",
            "cosmos"  => "Making Sagan proud",
            "ahoy"    => "Nautical terms like boats and whatnot",
            "lotr"    => "Lord of the Rings (more Tolkien)",
            "dogs"    => "200+ breeds can't be wrong",
            "patriot" => "Just like your uncle at Thanksgiving",
            "lorem"   => "Goode olde fashioned lorem ipsum (if you must)",
        );
        return $filesArray;
    }

    public function loadWords($wordGroup)
    {
        if ($this->isWordGroupName($wordGroup)) {
            $wordFile = __DIR__ . "/lang/" . strtolower($wordGroup) . ".json";
            if (file_exists($wordFile)) {
                $words = json_decode(file_get_contents($wordFile), TRUE);
                return $words;
            } else {
                //Word group file does not exist
                return FALSE;
            }
        } else {
            //Not a valid word group file name
            return FALSE;
        }
    }

    /*
     * isWordGroupName
     * Tests if given string is valid word group name.
     *
     * @param string $name
     * @return boolean
     */
    public function isWordGroupName($name)
    {
        $jsonFiles = $this->listWordFiles();
        if (array_key_exists($name, $jsonFiles)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /*
     * getFiller
     *
     * returns basic lorem-ipsum filler words for mixing with alternate word sources
     *
     * @access public
     * @param none
     * @return array
     */

    public function getFiller() {
        $filler = array(
            "consectetur",
            "adipisicing",
            "elit",
            "sed",
            "do",
            "eiusmod",
            "tempor",
            "incididunt",
            "ut",
            "labore",
            "et",
            "dolore",
            "magna",
            "aliqua",
            "ut",
            "enim",
            "ad",
            "minim",
            "veniam",
            "quis",
            "nostrud",
            "exercitation",
            "ullamco",
            "laboris",
            "nisi",
            "ut",
            "aliquip",
            "ex",
            "ea",
            "commodo",
            "consequat",
            "duis",
            "aute",
            "irure",
            "dolor",
            "in",
            "reprehenderit",
            "in",
            "voluptate",
            "velit",
            "esse",
            "cillum",
            "dolore",
            "eu",
            "fugiat",
            "nulla",
            "pariatur",
            "excepteur",
            "sint",
            "occaecat",
            "cupidatat",
            "non",
            "proident",
            "sunt",
            "in",
            "culpa",
            "qui",
            "officia",
            "deserunt",
            "mollit",
            "anim",
            "id",
            "est",
            "laborum"
        );
        return $filler;
    }

    /**
     * Word
     *
     * Generates a single word of Lotsa ipsum.
     *
     * @access public
     * @param  mixed $tags string or array of HTML tags to wrap output with
     * @return string generated Lotsa ipsum word
     */
    public function word($tags = FALSE)
    {
        return $this->words(1, $tags);
    }

    /**
     * Words Array
     *
     * Generates an array of Lotsa ipsum words.
     *
     * @access public
     * @param  integer $count how many words to generate
     * @param  mixed $tags string or array of HTML tags to wrap output with
     * @return array   generated Lotsa ipsum words
     */
    public function wordsArray($count = 1, $tags = FALSE)
    {
        return $this->words($count, $tags, TRUE);
    }

    /**
     * Words
     *
     * Generates words of Lotsa ipsum.
     *
     * @access public
     * @param  integer $count how many words to generate
     * @param  mixed $tags string or array of HTML tags to wrap output with
     * @param  boolean $array whether an array or a string should be returned
     * @return mixed   string or array of generated Lotsa ipsum words
     */
    public function words($count = 1, $tags = FALSE, $array = FALSE)
    {
        $words = array();
        $word_count = 0;

        // Shuffles and appends the word list to compensate for count
        // arguments that exceed the size of our vocabulary list
        while ($word_count < $count) {
            $shuffle = TRUE;

            while ($shuffle) {
                $this->shuffle();

                // Checks that the last word of the list and the first word of
                // the list that's about to be appended are not the same
                if (!$word_count || $words[$word_count - 1] != $this->words[0]) {
                    $words = array_merge($words, $this->words);
                    $word_count = count($words);
                    $shuffle = FALSE;
                }
            }
        }

        $words = array_slice($words, 0, $count);

        return $this->output($words, $tags, $array);
    }

    /**
     * Sentence
     *
     * Generates a full sentence of Lotsa ipsum.
     *
     * @access public
     * @param  mixed $tags string or array of HTML tags to wrap output with
     * @return string generated Lotsa ipsum sentence
     */
    public function sentence($tags = FALSE)
    {
        return $this->sentences(1, $tags);
    }

    /**
     * Sentences Array
     *
     * Generates an array of Lotsa ipsum sentences.
     *
     * @access public
     * @param  integer $count how many sentences to generate
     * @param  mixed $tags string or array of HTML tags to wrap output with
     * @return array   generated Lotsa ipsum sentences
     */
    public function sentencesArray($count = 1, $tags = FALSE)
    {
        return $this->sentences($count, $tags, TRUE);
    }

    /**
     * Sentences
     *
     * Generates sentences of Lotsa ipsum.
     *
     * @access public
     * @param  integer $count how many sentences to generate
     * @param  mixed $tags string or array of HTML tags to wrap output with
     * @param  boolean $array whether an array or a string should be returned
     * @return mixed   string or array of generated Lotsa ipsum sentences
     */
    public function sentences($count = 1, $tags = FALSE, $array = FALSE)
    {
        $sentences = array();

        for ($i = 0; $i < $count; $i++) {
            $sentences[] = $this->wordsArray($this->gauss(24.46, 5.08));
        }

        $this->punctuate($sentences);

        return $this->output($sentences, $tags, $array);
    }

    /**
     * Paragraph
     *
     * Generates a full paragraph of Lotsa ipsum.
     *
     * @access public
     * @param  mixed $tags string or array of HTML tags to wrap output with
     * @return string generated Lotsa ipsum paragraph
     */
    public function paragraph($tags = FALSE)
    {
        return $this->paragraphs(1, $tags);
    }

    /**
     * Paragraph Array
     *
     * Generates an array of Lotsa ipsum paragraphs.
     *
     * @access public
     * @param  integer $count how many paragraphs to generate
     * @param  mixed $tags string or array of HTML tags to wrap output with
     * @return array   generated Lotsa ipsum paragraphs
     */
    public function paragraphsArray($count = 1, $tags = FALSE)
    {
        return $this->paragraphs($count, $tags, TRUE);
    }

    /**
     * Paragraphss
     *
     * Generates paragraphs of Lotsa ipsum.
     *
     * @access public
     * @param  integer $count how many paragraphs to generate
     * @param  mixed $tags string or array of HTML tags to wrap output with
     * @param  boolean $array whether an array or a string should be returned
     * @return mixed   string or array of generated Lotsa ipsum paragraphs
     */
    public function paragraphs($count = 1, $tags = FALSE, $array = FALSE)
    {
        $paragraphs = array();

        for ($i = 0; $i < $count; $i++) {
            $paragraphs[] = $this->sentences($this->gauss(5.8, 1.93));
        }

        return $this->output($paragraphs, $tags, $array, "\n\n");
    }

    /**
     * Gaussian Distribution
     *
     * This is some smart kid stuff. I went ahead and combined the N(0,1) logic
     * with the N(m,s) logic into this single function. Used to calculate the
     * number of words in a sentence, the number of sentences in a paragraph
     * and the distribution of commas in a sentence.
     *
     * @access private
     * @param  double $mean average value
     * @param  double $std_dev stadnard deviation
     * @return double  calculated distribution
     */
    private function gauss($mean, $std_dev)
    {
        $x = mt_rand() / mt_getrandmax();
        $y = mt_rand() / mt_getrandmax();
        $z = sqrt(-2 * log($x)) * cos(2 * pi() * $y);

        return $z * $std_dev + $mean;
    }

    /**
     * Shuffle
     *
     * Shuffles the words, forcing the first words at the beginning to be 'lotsa ipsum' if it is
     * the first time we are generating the text.
     *
     * @access private
     */
    private function shuffle()
    {
        if ($this->first) {
            $this->first = array("lotsa","ipsum");
            shuffle($this->words);
            $this->words = $this->first + $this->words;
            $this->first = FALSE;
        } else {
            shuffle($this->words);
        }
    }

    /**
     * Punctuate
     *
     * Applies punctuation to a sentence. This includes a period at the end,
     * the injection of commas as well as capitalizing the first letter of the
     * first word of the sentence.
     *
     * @access private
     * @param  array $sentences the sentences we would like to punctuate
     */
    private function punctuate(&$sentences)
    {
        foreach ($sentences as $key => $sentence) {
            $words = count($sentence);

            // Only worry about commas on sentences longer than 4 words
            if ($words > 4) {
                $mean = log($words, 6);
                $std_dev = $mean / 6;
                $commas = round($this->gauss($mean, $std_dev));

                for ($i = 1; $i <= $commas; $i++) {
                    $word = round($i * $words / ($commas + 1));

                    if ($word < ($words - 1) && $word > 0) {
                        $sentence[$word] .= ',';
                    }
                }
            }

            $sentences[$key] = ucfirst(implode(' ', $sentence) . '.');
        }
    }

    /**
     * Output
     *
     * Does the rest of the processing of the strings. This includes wrapping
     * the strings in HTML tags, handling transformations with the ability of
     * back referencing and determining if the passed array should be converted
     * into a string or not.
     *
     * @access private
     * @param  array $strings an array of generated strings
     * @param  mixed $tags string or array of HTML tags to wrap output with
     * @param  boolean $array whether an array or a string should be returned
     * @param  string $delimiter the string to use when calling implode()
     * @return mixed   string or array of generated Lotsa ipsum text
     */
    private function output($strings, $tags, $array, $delimiter = ' ')
    {
        if ($tags) {
            if (!is_array($tags)) {
                $tags = array($tags);
            } else {
                // Flips the array so we can work from the inside out
                $tags = array_reverse($tags);
            }

            foreach ($strings as $key => $string) {
                foreach ($tags as $tag) {
                    // Detects / applies back reference
                    if ($tag[0] == '<') {
                        $string = str_replace('$1', $string, $tag);
                    } else {
                        $string = sprintf('<%1$s>%2$s</%1$s>', $tag, $string);
                    }

                    $strings[$key] = $string;
                }
            }
        }

        if (!$array) {
            $strings = implode($delimiter, $strings);
        }

        return $strings;
    }
}

